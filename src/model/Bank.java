package model;

public class Bank {
	
	BankAccount b = new BankAccount();
	public  void deposit(double amount){
		b.deposit(amount);
	}
	
	public void withdraw(double amount){
		b.withdraw(amount);
	}
	
	public double checkBalance(){
		return b.getBalance();
	}

	public String toString(double money) {
		return money+" ";
	}
	
}
