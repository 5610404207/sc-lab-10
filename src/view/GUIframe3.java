package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUIframe3 extends JFrame {
	JCheckBox redButton = new JCheckBox("RED");
	JCheckBox greenButton = new JCheckBox("GREEN");
	JCheckBox blueButton = new JCheckBox("BLUE");
	JPanel j1,j2;

	public GUIframe3() {
		createFrame();
	}
	
	public void createFrame() {
		j1 = new JPanel();
		j1.setLayout(new FlowLayout());
		add(j1, BorderLayout.SOUTH);
		j2 = new JPanel();
		j2.setLayout(new FlowLayout());
		add(j2, BorderLayout.CENTER);
		
		j1.add(redButton);
		j1.add(greenButton);
		j1.add(blueButton);
		
		class ChoiceListener implements ActionListener{
			public void actionPerformed(ActionEvent event) {
				setBackground();
			}
		}
		ActionListener listener = new ChoiceListener();
		
		redButton.addActionListener(listener);
		greenButton.addActionListener(listener);
		blueButton.addActionListener(listener);
	}

		
	private void setBackground() {
		if (redButton.isSelected()) {
			j2.setBackground(Color.RED);
		}
		if (greenButton.isSelected()){
			j2.setBackground(Color.GREEN);
		}
		if (blueButton.isSelected()) {
			j2.setBackground(Color.BLUE);
		}
		if (redButton.isSelected() && greenButton.isSelected()) {
			j2.setBackground(Color.YELLOW);
		}
		if (greenButton.isSelected() && blueButton.isSelected()) {
			j2.setBackground(Color.CYAN);
		}
		if (redButton.isSelected() && blueButton.isSelected()) {
			j2.setBackground(Color.MAGENTA);
		}
		if (redButton.isSelected() && greenButton.isSelected()&& blueButton.isSelected()) {
			j2.setBackground(Color.WHITE);
		}
	}
}
