package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JRadioButton;


public class GUIframe2 extends JFrame {

	
	public GUIframe2() {
		createFrame();
	}

	public void createFrame() {
		
	    JPanel j1 = new JPanel();
	    j1.setLayout(new FlowLayout());
	    add(j1, BorderLayout.SOUTH);
		JPanel j2 = new JPanel();
		j2.setLayout(new FlowLayout());
	    add(j2, BorderLayout.CENTER);
		
		
	    JRadioButton redButton = new JRadioButton("RED");
	    JRadioButton greenButton = new JRadioButton("GREEN");
	    JRadioButton blueButton = new JRadioButton("BLUE");
        j1.add(redButton);
        j1.add(greenButton);
        j1.add(blueButton);
        
        ButtonGroup group = new ButtonGroup();
        group.add(redButton);
        group.add(greenButton);
        group.add(blueButton);
        
        
        redButton.addActionListener(new ActionListener() {
        	   public void actionPerformed(ActionEvent event) {
        	    j2.setBackground(Color.RED);
        	   }
        	  });
        greenButton.addActionListener(new ActionListener() {
     	   public void actionPerformed(ActionEvent event) {
     	    j2.setBackground(Color.GREEN);
     	   }
     	  });
        blueButton.addActionListener(new ActionListener() {
     	   public void actionPerformed(ActionEvent event) {
     	    j2.setBackground(Color.BLUE);
     	   }
     	  });
  
    }


	
	
}
