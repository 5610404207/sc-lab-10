package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUIframe4 extends JFrame {
	JComboBox<String> combo = new JComboBox<>();
	JPanel j1,j2;

	public GUIframe4() {
		createFrame();
	}
	
	public void createFrame() {
		combo.addItem("RED");
		combo.addItem("GREEN");
		combo.addItem("BLUE");
		j1 = new JPanel();
		j1.setLayout(new FlowLayout());
		add(j1, BorderLayout.SOUTH);
		j2 = new JPanel();
		j2.setLayout(new FlowLayout());
		add(j2, BorderLayout.CENTER);
		
		j1.add(combo);
		
		class ChoiceListener implements ActionListener{
			public void actionPerformed(ActionEvent event) {
				setBackground();
				
			}
		}
		ActionListener listener = new ChoiceListener();
		
		combo.addActionListener(listener);
		
	}

		
	private void setBackground() {
		if (combo.getSelectedItem().equals("RED")){
			j2.setBackground(Color.RED);
		}
		if (combo.getSelectedItem().equals("GREEN")){
			j2.setBackground(Color.GREEN);
		}
		if (combo.getSelectedItem().equals("BLUE")) {
			j2.setBackground(Color.BLUE);
		}
	}
}
