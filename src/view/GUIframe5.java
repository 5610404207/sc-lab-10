package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class GUIframe5 extends JFrame {
	JMenu menu = new JMenu("Color");
	JPanel j2;
	JMenuBar menuBar;
	JMenu jmenu;
	JMenuItem menuRed, menuGreen, menuBlue;

	public GUIframe5() {

		createFrame();

	}


	public void createFrame() {
		menuBar = new JMenuBar();
		jmenu = new JMenu("Color");
		menuRed = new JMenuItem("RED");
		menuGreen = new JMenuItem("GREEN");
		menuBlue = new JMenuItem("BLUE");

	
		j2 = new JPanel();
		j2.setLayout(new FlowLayout());
		add(j2, BorderLayout.CENTER);

		jmenu.add(menuRed);
		jmenu.add(menuGreen);
		jmenu.add(menuBlue);

		menuBar.add(jmenu);
		setJMenuBar(menuBar);



		menuRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				j2.setBackground(Color.RED);
			}
		});
		menuGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				j2.setBackground(Color.GREEN);
			}
		});
		menuBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				j2.setBackground(Color.BLUE);
			}
		});
	}
}
