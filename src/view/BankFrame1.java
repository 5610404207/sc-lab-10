package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Bank;

public class BankFrame1 extends JFrame {
	JPanel panel1, panel2, panel3;

	public BankFrame1() {
		createFrame();

	}

	public void createFrame() {

		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());

		JLabel label1 = new JLabel();
		label1.setText("Input : ");
		panel1.add(label1);
		add(panel1, BorderLayout.NORTH);
		JTextField text = new JTextField(10);
		panel1.add(text);

		JLabel label2 = new JLabel();
		label2.setText("Balance = ");
		add(label2);

		panel3 = new JPanel();
		JButton button1 = new JButton("Deposit");
		JButton button2 = new JButton("Withdraw");
		JButton button3 = new JButton("Check Balance");

		panel3.setLayout(new FlowLayout());
		panel3.add(button1);
		panel3.add(button2);
		panel3.add(button3);
		add(panel3, BorderLayout.SOUTH);

		Bank b = new Bank();
		
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String amount = text.getText();
				double money = Integer.parseInt(amount);
				if (money<0) {
					label2.setText("Incorrect input");
				}else{
					b.deposit(money);
				}
				double Balance = b.checkBalance();
				label2.setText("Balance is " + b.toString(Balance));
			}

		});

		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String amount = text.getText();
				double money = Integer.parseInt(amount);
				double Balance = b.checkBalance();
				if (Balance <= 0 && Balance < money) {
					label2.setText("Not enough money");
				} else {
					b.withdraw(money);
					label2.setText("Balance is " + b.toString(Balance));
				}
			}

		});
		
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double Balance = b.checkBalance();
				label2.setText("Balance is "+ b.toString(Balance));
			}

		});
	}
}
